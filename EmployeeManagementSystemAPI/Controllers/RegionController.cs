﻿using EmployeeManagementSystemAPI.Models;
using AutoMapper;
using EmployeeManagementSystemAPI.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EmployeeManagementSystemAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegionController : ControllerBase
    {
        private readonly EmployeeDbContext _context;
        private readonly RegionRepository _RR;
        private readonly IMapper _mapper;

        public RegionController(EmployeeDbContext context, IMapper mapper, RegionRepository RR)
        {
            _context = context;
            _mapper = mapper;
            _RR = RR;
        }
        // GET: api/<DepartmentController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Region>>> GetRegionDetail()
        {
            return await _context.Region.ToListAsync();
        }

        [HttpPost]
        public async Task<IActionResult> AddRegionAsync([FromBody] RegionDTO regDTO)
        {
            var reg = _mapper.Map<RegionDTO, Region>(regDTO);
            var regdto = await _RR.AddRegion(reg);
            return new OkObjectResult(regdto);
        }

        [HttpPut("{Id}")]
        public async Task<IActionResult> UpdateRegionAsync([FromBody] RegionDTO relDTO)
        {
            var rel = _mapper.Map<RegionDTO, Region>(relDTO);
            var relDTOo = await _RR.UpdateRegion(rel);
            return new OkObjectResult(relDTOo);
        }

        [HttpDelete("{regID}")]
        public bool DeleteRegionAsync(int regID)
        {
            return _RR.DeleteRegion(regID);
        }


    }
}
