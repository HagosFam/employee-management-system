﻿using EmployeeManagementSystemAPI.Models;
using EmployeeManagementSystemAPI.Repository;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EmployeeManagementSystemAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class KebelleController : ControllerBase
    {
        private readonly EmployeeDbContext _context;
        private readonly KebelleRepository _KR;
        private readonly IMapper _mapper;
        public KebelleController(EmployeeDbContext context, KebelleRepository kebelleR, IMapper mapper)
        {
            _context = context;
            _KR = kebelleR;
            _mapper = mapper;
        }
        // GET: api/<KebelleController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Kebelle>>> GetKebelleDetail()
        {
            return await _context.Kebelle.ToListAsync();
        }

        [HttpPost]
        public async Task<IActionResult> AddKebelleAsync([FromBody] KebelleDTO kebDTO)
        {
            var keb = _mapper.Map<KebelleDTO, Kebelle>(kebDTO);
            var kebDto = await _KR.AddKebelle(keb);
            return new OkObjectResult(kebDto);
        }

        [HttpPut("{Id}")]
        public async Task<IActionResult> UpdateKebelleAsync([FromBody] KebelleDTO kebDTO)
        {
            var keb = _mapper.Map<KebelleDTO, Kebelle>(kebDTO);
            var kebDto = await _KR.UpdateKebelle(keb);
            return new OkObjectResult(kebDto);
        }

        [HttpDelete("{kebID}")]
        public bool DeleteKebelleAsync(int kebID)
        {
            return _KR.DeleteKebelle(kebID);
        }
    }
}
