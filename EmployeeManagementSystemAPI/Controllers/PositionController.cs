﻿using EmployeeManagementSystemAPI.Models;
using EmployeeManagementSystemAPI.Repository;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EmployeeManagementSystemAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PositionController : ControllerBase
    {
        private readonly EmployeeDbContext _context;
        private readonly PositionRepository _PR;
        private readonly IMapper _mapper;

        public PositionController(EmployeeDbContext context, PositionRepository PR, IMapper mapper)
        {
             _PR = PR;
            _context = context;
            _mapper = mapper;
        }
        // GET: api/<DepartmentController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Position>>> GetPositionDetail()
        {
            return await _context.Position.ToListAsync();
        }

        // GET: api/<KebelleController>/id
        [HttpGet("{id}")]
        public async Task<ActionResult<Position>> GetPositionDetailById(int id)
        {
            var positionDetail = await _context.Position.FindAsync(id);

            if (positionDetail == null)
            {
                return NotFound();
            }
            return positionDetail;
        }

        [HttpPost]
        public async Task<IActionResult> AddPositionAsync([FromBody] PositionDTO posDTO)
        {
            var pos = _mapper.Map<PositionDTO, Position>(posDTO);
            var poDTO = await _PR.AddPosition(pos);
            return new OkObjectResult(poDTO);
        }

        [HttpPut("{Id}")]
        public async Task<IActionResult> UpdatePositionAsync([FromBody] PositionDTO posDTO)
        {
            var pos = _mapper.Map<PositionDTO, Position>(posDTO);
            var posdto = await _PR.UpdatePosition(pos);
            return new OkObjectResult(posdto);
        }

        [HttpDelete("{posID}")]
        public bool DeletePositionAsync(int posID)
        {
            return _PR.DeletePosition(posID);
        }
    }
}
