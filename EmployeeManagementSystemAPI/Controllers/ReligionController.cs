﻿using AutoMapper;
using EmployeeManagementSystemAPI.Repository;
using EmployeeManagementSystemAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EmployeeManagementSystemAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReligionController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ReligionRepository _RR;
        private readonly EmployeeDbContext _context;

        public ReligionController(EmployeeDbContext context, ReligionRepository RR, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
            _RR = RR;

        }
        // GET: api/<DepartmentController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Religion>>> GetReligionDetail()
        {
            return await _context.Religion.ToListAsync();
        }

        [HttpPost]
        public async Task<IActionResult> AddReligionAsync([FromBody] ReligionDTO relDTO)
        {
            var rel = _mapper.Map<ReligionDTO, Religion>(relDTO);
            var reldto = await _RR.AddReligion(rel);
            return new OkObjectResult(reldto);
        }

        [HttpPut("{Id}")]
        public async Task<IActionResult> UpdateReligionAsync([FromBody] ReligionDTO relDTO)
        {
            var rel = _mapper.Map<ReligionDTO, Religion>(relDTO);
            var relDTOo = await _RR.UpdateReligion(rel);
            return new OkObjectResult(relDTOo);
        }

        [HttpDelete("{relID}")]
        public bool DeleteReligionAsync(int relID)
        {
            return _RR.DeleteReligion(relID);
        }


    }
}
