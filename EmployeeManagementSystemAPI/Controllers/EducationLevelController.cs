﻿using EmployeeManagementSystemAPI.Models;
using AutoMapper;
using EmployeeManagementSystemAPI.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EmployeeManagementSystemAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EducationLevelController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly EducationalLevelRepository ElevelR;
        private readonly EmployeeDbContext _context;

        public EducationLevelController(EducationalLevelRepository IelevelR, IMapper mapper, EmployeeDbContext context)
        {
            ElevelR = IelevelR; //I means injected
            _context = context;
            _mapper = mapper;
        }

        // GET: api/<EducationlLevelController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<EducationLevel>>> GetPositionDetail()
        {
            return await _context.EducationLevel.ToListAsync();
        }

        [HttpPost]
        public async Task<IActionResult> AddEducationalLevelAsync([FromBody] EducationLevelDTO ElevelDTO)
        {
            var level = _mapper.Map<EducationLevelDTO, EducationLevel>(ElevelDTO);
            var ELevelDTO = await ElevelR.AddEducationLevel(level);
            return new OkObjectResult(ELevelDTO);
        }

        [HttpPut("{Id}")]
        public async Task<IActionResult> UpdateEducationLevelAsync([FromBody] EducationLevelDTO ElevelDTO)
        {
            var Elevel = _mapper.Map<EducationLevelDTO, EducationLevel>(ElevelDTO);
            var EdulevelDTO = await ElevelR.UpdateEducationLevel(Elevel);
            return new OkObjectResult(EdulevelDTO);
        }

        [HttpDelete("{EduLevelId}")]
        public bool DeleteEduLevelAsync(int EduLevelId)
        {
            return ElevelR.DeleteEducationalLevel(EduLevelId);
        }
    }
}
