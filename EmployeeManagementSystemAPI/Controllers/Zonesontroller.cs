﻿using AutoMapper;
using EmployeeManagementSystemAPI.Repository;
using EmployeeManagementSystemAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EmployeeManagementSystemAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Zonesontroller : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly EmployeeDbContext _context;
        private readonly ZoneRepository _ZR;

        public Zonesontroller(EmployeeDbContext context, IMapper mapper, ZoneRepository ZR)
        {
            _context = context;
            _mapper = mapper;
            _ZR = ZR;
        }
        // GET: api/<DepartmentController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Zone>>> GetZoneDetail()
        {
            return await _context.Zone.ToListAsync();
        }

        [HttpPost]
        public async Task<IActionResult> AddZoneAsync([FromBody] ZoneDTO zDTO)
        {
            var emp = _mapper.Map<ZoneDTO, Zone>(zDTO);
            var empDTO = await _ZR.AddZone(emp);
            return new OkObjectResult(empDTO);
        }

        [HttpPut("{Id}")]
        public async Task<IActionResult> UpdateZoneAsyc([FromBody] ZoneDTO zDTO)
        {
            var zdtoo = _mapper.Map<ZoneDTO, Zone>(zDTO);
            var zzDto = await _ZR.UpdateZone(zdtoo);
            return new OkObjectResult(zzDto);
        }

        [HttpDelete("{ZoneID}")]
        public bool DeleteZoneAsync(int ZoneID)
        {
            return _ZR.DeleteZone(ZoneID);
        }

    }
}
