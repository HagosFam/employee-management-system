﻿using EmployeeManagementSystemAPI.Models;
using EmployeeManagementSystemAPI.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EmployeeManagementSystemAPI.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly EmployeeManagementRepository _employeeManagement;
         public EmployeeController(EmployeeManagementRepository employeeManagement, IMapper mapper)
         {
            _employeeManagement = employeeManagement; //I means injected
             _mapper = mapper;
         }

        [HttpGet]
        public async Task<IEnumerable<Employees>> GetAllEmployeeList()
        {
            var employees = await _employeeManagement.GetAllEmployeeList();
            return employees;
        }

        [HttpPost]
        public async Task<IActionResult> AddEmployeeAsync([FromBody] EmployeeDTO eDTO)
        {
            var emp = _mapper.Map<EmployeeDTO, Employees>(eDTO);
            var empDTO = await _employeeManagement.AddEmployee(emp);
            return new OkObjectResult(empDTO);
        }

        [HttpPut("{Id}")]
        public async Task<IActionResult> UpdateEmployeeAsyc([FromBody] EmployeeDTO eDTO)
        {
            var EDTO = _mapper.Map<EmployeeDTO, Employees>(eDTO);
            var eeDto = await _employeeManagement.UpdateEmployee(EDTO);
            return new OkObjectResult(eeDto);
        }

        [HttpDelete("{empId}")]
        public bool DeleteEmployeeAsync(int empId)
        {
            return _employeeManagement.DeleteEmployee(empId);
        }

    }

}
