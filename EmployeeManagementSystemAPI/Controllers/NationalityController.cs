﻿using EmployeeManagementSystemAPI.Models;
using EmployeeManagementSystemAPI.Repository;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EmployeeManagementSystemAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NationalityController : ControllerBase
    {
        private readonly EmployeeDbContext _context;
        private readonly IMapper _mapper;
        private readonly NationalityRepository _NR;

        public NationalityController(EmployeeDbContext context, IMapper mapper, NationalityRepository NR)
        {
            _context = context;
            _mapper = mapper;
            _NR = NR;
        }
        // GET: api/<DepartmentController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Nationality>>> GetNationalityDetail()
        {
            return await _context.Nationality.ToListAsync();
        }

        [HttpPost]
        public async Task<IActionResult> AddNationalityAsync([FromBody] NationalityDTO naDTO)
        {
            var nat = _mapper.Map<NationalityDTO, Nationality>(naDTO);
            var natDTO = await _NR.AddNationality(nat);
            return new OkObjectResult(natDTO);
        }

        [HttpPut("{Id}")]
        public async Task<IActionResult> UpdateNationalityAsync([FromBody] NationalityDTO natDTO)
        {
            var nat = _mapper.Map<NationalityDTO, Nationality>(natDTO);
            var natdto = await _NR.UpdateNationality(nat);
            return new OkObjectResult(natdto);
        }

        [HttpDelete("{natDTO}")]
        public bool DeleteNationalityAsync(int natDTO)
        {
            return _NR.DeleteNationality(natDTO);
        }
    }
}
