﻿using AutoMapper;
using EmployeeManagementSystemAPI.Models;
using EmployeeManagementSystemAPI.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EmployeeManagementSystemAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DepartmentController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly DepartmentRepository _ER;
        private readonly EmployeeDbContext _context;


        public DepartmentController(DepartmentRepository ER, IMapper mapper, EmployeeDbContext context)
        {
            _ER = ER; //I means injected
            _context = context;
            _mapper = mapper;
        }
        // GET: api/<DepartmentController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Department>>> GetDepartmentDetail()
        {
            return await _context.Department.ToListAsync();
        }

        [HttpPost]
        public async Task<IActionResult> AddDepartmentAsync([FromBody] DepartmentDTO DeptDTO)
        {
            try
            {
                var dept = _mapper.Map<DepartmentDTO, Department>(DeptDTO);
                var deptDto = await _ER.AddDepartment(dept);
                return new OkObjectResult(deptDto);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }

        [HttpPut("{DeptId}")]
        public async Task<IActionResult> UpdateDepartmentAsync([FromBody] DepartmentDTO DeptDTO)
        {
            var dep = _mapper.Map<DepartmentDTO, Department>(DeptDTO);
            var depdto = await _ER.UpdateDepartment(dep);
            return new OkObjectResult(depdto);
        }

        [HttpDelete("{DeptId}")]
        public bool DeleteDepartmentAsync(int DeptID)
        {
            return _ER.DeleteDepartment(DeptID);
        }

    }
}
