﻿using AutoMapper;
using EmployeeManagementSystemAPI.Repository;
using EmployeeManagementSystemAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EmployeeManagementSystemAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WoredaController : ControllerBase
    {
        private readonly WoredaRepository _WR;
        private readonly IMapper _mapper;
        private readonly EmployeeDbContext _context;

        public WoredaController(EmployeeDbContext context, WoredaRepository WR, IMapper mapper)
        {
            _context = context;
            _WR = WR;
            _mapper = mapper;
        }
        // GET: api/<DepartmentController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Woreda>>> GetWoredaDetail()
        {
            return await _context.Woreda.ToListAsync();
        }

        [HttpPost]
        public async Task<IActionResult> AddDWoredaAsync([FromBody] WoredaDTO werDTO)
        {
            var wer = _mapper.Map<WoredaDTO, Woreda>(werDTO);
            var weredaDto = await _WR.AddWoreda(wer);
            return new OkObjectResult(weredaDto);
        }

        [HttpPut("{Id}")]
        public async Task<IActionResult> UpdateWoredaAsync([FromBody] WoredaDTO werDTO)
        {
            var woreda = _mapper.Map<WoredaDTO, Woreda>(werDTO);
            var weredaDTO = await _WR.UpdateWoreda(woreda);
            return new OkObjectResult(weredaDTO);
        }

        [HttpDelete("{werId}")]
        public bool DeleteWoredaAsync(int werId)
        {
            return _WR.DeleteWoreda(werId);
        }

    }
}
