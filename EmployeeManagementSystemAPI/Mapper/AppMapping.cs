﻿using AutoMapper;
using EmployeeManagementSystemAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeManagementSystemAPI.Mapper
{
    public class AppMapping: Profile 
    {
        public AppMapping()
        {
            CreateMap<Department, DepartmentDTO>().ReverseMap();
            CreateMap<EducationLevel, EducationLevelDTO>().ReverseMap();
            CreateMap<Employees, EmployeeDTO>().ReverseMap();
            CreateMap<Kebelle, KebelleDTO>().ReverseMap();
            CreateMap<Nationality, NationalityDTO>().ReverseMap();
            CreateMap<Position, PositionDTO>().ReverseMap();
            CreateMap<Region, RegionDTO>().ReverseMap();
            CreateMap<Religion, ReligionDTO>().ReverseMap();
            CreateMap<Woreda, WoredaDTO>().ReverseMap();
            CreateMap<Zone, ZoneDTO>().ReverseMap();
            //Add the rest of mappings here
        }
    }
}
