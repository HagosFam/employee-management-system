﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeManagementSystemAPI.Models
{

    public class EmployeeDbContext : DbContext
    {
        public EmployeeDbContext(DbContextOptions<EmployeeDbContext> options) : base(options)
        {
        }
        public DbSet<Employees> Employees { get; set; }
        public DbSet<Department> Department { get; set; }
        public DbSet<EducationLevel> EducationLevel { get; set; }
        public DbSet<Kebelle> Kebelle { get; set; }
        public DbSet<Nationality> Nationality { get; set; }
        public DbSet<Position> Position { get; set; }
        public DbSet<Region> Region { get; set; }
        public DbSet<Religion> Religion { get; set; }
        public DbSet<Woreda> Woreda { get; set; }
        public DbSet<Zone> Zone { get; set; }
    }
}
