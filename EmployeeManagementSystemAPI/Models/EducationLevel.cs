﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeManagementSystemAPI.Models
{
    public class EducationLevel
    {
        [Key]
        public int EducationLevelId { get; set; }
        [Required(ErrorMessage = "Education level is required")]
        [StringLength(20, ErrorMessage = "Educaiton level name should be max of 20 words")]
        public string EducationLevelName { get; set; }
    }

    public class EducationLevelDTO
    {
        public int EducationLevelId { get; set; }
        public string EducationLevelName { get; set; }
    }
}
