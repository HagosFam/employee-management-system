﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeManagementSystemAPI.Models
{
    public class Kebelle
    {
        [Key]
        public int KebelleId { get; set; }

        [Required(ErrorMessage ="Woreda ID is required")]
        public int WoredaId { get; set; }

        [Required(ErrorMessage= "Kebelle name is required")]
        public string KebelleName { get; set; }
    }

    public class KebelleDTO
    {
        public int KebelleId { get; set; }
        public int WoredaId { get; set; }
        public string KebelleName { get; set; }
    }
}
