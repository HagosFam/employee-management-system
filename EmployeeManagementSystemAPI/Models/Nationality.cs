﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeManagementSystemAPI.Models
{
    public class Nationality
    {
        [Key]
        public int NationalityId { get; set; }
        [Required(ErrorMessage = "Nationality name is required")]
        public string NationalityName { get; set; }
    }

    public class NationalityDTO
    {
        public int NationalityId { get; set; }
        public string NationalityName { get; set; }
    }
}
