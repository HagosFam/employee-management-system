﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeManagementSystemAPI.Models
{
    public class Employees
    {


        [Key]
        public int StaffId { get; set; }
        [Required]
        public int UnitId { get; set; }
        [Required]
        public string FirstName { get; set; }
        public string FatherName { get; set; }
        public string GrandName { get; set; }
                //enum 0=male and 1=femele
        public int Sex { get; set; }
        public int Religion { get; set; }
        public DateTime BirthDate { get; set; }
        public string BirthPlace { get; set; }
        //pulled from enumaration 
        public int MeritialStatusId { get; set; }
        public int NationalityId { get; set; }
        public int RegionId { get; set; }
        public int ZoneId { get; set; }
        public int WoredaId { get; set; }
        public int KebeleId { get; set; }
        [EmailAddress][Required]
        public string Email { get; set; }
        [Required]
        public string TelMobile { get; set; }
        public string TelHome { get; set; }
        public string TelOffDirect1 { get; set; }
        public string TelOffDirect2 { get; set; }
        public string TelOffExt1 { get; set; }
        public string RoomNo { get; set; }
        public int EducationLevel { get; set; }
        public string EducationField { get; set; }
        [Required]
        public int PositionId { get; set; }
        [Required]
        public float Salary { get; set; }
        public DateTime EmpDate { get; set; }
        public string EmpLetter { get; set; }
        public int EmpStatus { get; set; }
        public int CurrentStatus { get; set; }
        public DateTime TerminationDate { get; set; }
        public string TerminationReason { get; set; }
        public string TerminationLetter { get; set; }
        //i didn't get the data type for the time being 
        public byte[] Photo { get; set; }
    }

    public class EmployeeDTO
    {
        public int StaffId { get; set; }
        public int UnitId { get; set; }
        public string FirstName { get; set; }
        public string FatherName { get; set; }
        public string GrandName { get; set; }
        //enum 0=male and 1=femele
        public int Sex { get; set; }
        public int Religion { get; set; }
        public DateTime BirthDate { get; set; }
        public string BirthPlace { get; set; }
        //pulled from enumaration 
        public int MeritialStatusId { get; set; }
        public int NationalityId { get; set; }
        public int RegionId { get; set; }
        public int ZoneId { get; set; }
        public int WoredaId { get; set; }
        public int KebeleId { get; set; }
        public string Email { get; set; }
        public string TelMobile { get; set; }
        public string TelHome { get; set; }
        public string TelOffDirect1 { get; set; }
        public string TelOffDirect2 { get; set; }
        public string TelOffExt1 { get; set; }
        public string RoomNo { get; set; }
        public int EducationLevel { get; set; }
        public string EducationField { get; set; }
        public int PositionId { get; set; }
        public float Salary { get; set; }
        public DateTime EmpDate { get; set; }
        public string EmpLetter { get; set; }
        public int EmpStatus { get; set; }
        public int CurrentStatus { get; set; }
        public DateTime TerminationDate { get; set; }
        public string TerminationReason { get; set; }
        public string TerminationLetter { get; set; }
        //i didn't get the data type for the time being 
        public byte[] Photo { get; set; }
    }
}
