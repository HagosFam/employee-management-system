﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeManagementSystemAPI.Models
{
    public class Position
    {
        [Key]
        public int PositionId { get; set; }
        [Required(ErrorMessage ="Postion name is required")]
        public string PositionName { get; set; }
    }

    public class PositionDTO
    {
        public int PositionId { get; set; }
        public string PositionName { get; set; }
    }
}
