﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeManagementSystemAPI.Models
{
    public class Region
    {
        [Key]
        public int RegionId { get; set; }
        [Required(ErrorMessage = "Region name is required")]
        public string RegionName { get; set; }
    }

    public class RegionDTO
    {
        public int RegionId { get; set; }
        public string RegionName { get; set; }
    }
}
