﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeManagementSystemAPI.Models
{
    public class Woreda
    {
        [Key]
        public int WoredaId { get; set; }
        public int ZoneId { get; set; }
        public string WoredaName { get; set; }
    }

    public class WoredaDTO
    {
        public int WoredaId { get; set; }
        public int ZoneId { get; set; }
        public string WoredaName { get; set; }
    }
}
