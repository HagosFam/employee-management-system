﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeManagementSystemAPI.Models
{
    public class Zone
    {
        [Key]
        public int ZoneId { get; set; }
        public int RegionId { get; set; }
        public string ZoneName { get; set; }
    }

    public class ZoneDTO
    {
        public int ZoneId { get; set; }
        public int RegionId { get; set; }
        public string ZoneName { get; set; }
    }
}
