﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeManagementSystemAPI.Models
{
    public class Religion
    {
        [Key]
        public int ReligionId { get; set; }
        [Required(ErrorMessage ="Religion name is required ")]
        [StringLength(20, ErrorMessage = "Regligion name must be less than 20")]
        public string ReligionName { get; set; }
    }

    public class ReligionDTO
    {
        public int ReligionId { get; set; }
        public string ReligionName { get; set; }
    }


}
