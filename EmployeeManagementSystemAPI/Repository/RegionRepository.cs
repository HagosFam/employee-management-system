﻿using EmployeeManagementSystemAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeManagementSystemAPI.Repository
{
    public class RegionRepository
    {
        private readonly EmployeeDbContext _context;
        public RegionRepository(EmployeeDbContext context)
        {
            _context = context;
        }
        public async Task<bool> UpdateRegion(Region reg)
        {
            try
            {
                _context.Update(reg);
                _context.Entry(reg).Property(x => x.RegionId).IsModified = false;
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                string s = ex.Message;
                return false;
            }
        }

        public async Task<bool> AddRegion(Region region)
        {
            _context.Region.Add(region);
            await _context.SaveChangesAsync();
            return true;
        }

        public bool DeleteRegion(int RegId)
        {
            Region reg = _context.Region.Where(x => x.RegionId == RegId).FirstOrDefault();

            if (reg != null)
            {
                _context.Region.Remove(reg);
                _context.SaveChangesAsync();
                return true;
            }
            return false;
        }
    }


}
