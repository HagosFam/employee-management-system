﻿using EmployeeManagementSystemAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeManagementSystemAPI.Repository
{
    public class DepartmentRepository
    {
        private readonly EmployeeDbContext _context;
        public DepartmentRepository(EmployeeDbContext context)
        {
            _context = context;
        }

        public async Task<bool> UpdateDepartment(Department Dep)
        {
            try
            {
                _context.Update(Dep);
                _context.Entry(Dep).Property(x => x.DepartmentId).IsModified = false;
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                string s = ex.Message;
                return false;
            }
        }

        public async Task<bool> AddDepartment(Department Dep)
        {
            _context.Department.Add(Dep);
            await _context.SaveChangesAsync();
            return true;
        }

        public bool DeleteDepartment(int DeptId)
        {
            Department dept = _context.Department.Where(x => x.DepartmentId == DeptId).FirstOrDefault();
            if (dept != null)
            {
                _context.Department.Remove(dept);
                _context.SaveChangesAsync();
                return true;
            }
            return false;
        }
    }


}
