﻿using EmployeeManagementSystemAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeManagementSystemAPI.Repository
{
    public class ZoneRepository
    {
        private readonly EmployeeDbContext _context;
        public ZoneRepository(EmployeeDbContext context)
        {
            _context = context;
        }

        public async Task<bool> UpdateZone(Zone zone)
        {
            try
            {
                _context.Update(zone);
                _context.Entry(zone).Property(x => x.ZoneId).IsModified = false;
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                string s = ex.Message;
                return false;
            }
        }

        public async Task<bool> AddZone(Zone zon)
        {
            _context.Zone.Add(zon);
            await _context.SaveChangesAsync();
            return true;
        }

        public bool DeleteZone(int zoneId)
        {
            Zone zon = _context.Zone.Where(x => x.ZoneId == zoneId).FirstOrDefault();

            if (zon != null)
            {
                _context.Zone.Remove(zon);
                _context.SaveChangesAsync();
                return true;
            }
            return false;
        }
    }
}
