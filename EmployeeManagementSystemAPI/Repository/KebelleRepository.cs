﻿using EmployeeManagementSystemAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeManagementSystemAPI.Repository
{
    public class KebelleRepository
    {
        private readonly EmployeeDbContext _context;
        public KebelleRepository(EmployeeDbContext context)
        {
            _context = context;
        }

        public async Task<bool> UpdateKebelle(Kebelle Kebe)
        {
            try
            {
                _context.Update(Kebe);
                _context.Entry(Kebe).Property(x => x.KebelleId).IsModified = false;
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                string s = ex.Message;
                return false;
            }
        }

        public async Task<bool> AddKebelle(Kebelle Kebe)
        {
            _context.Kebelle.Add(Kebe);
            await _context.SaveChangesAsync();
            return true;
        }

        public bool DeleteKebelle(int KebelleId)
        {
            Kebelle kebe = _context.Kebelle.Where(x => x.KebelleId == KebelleId).FirstOrDefault();

            if (kebe != null)
            {
                _context.Kebelle.Remove(kebe);
                _context.SaveChangesAsync();
                return true;
            }
            return false;
        }
    }
}
