﻿using EmployeeManagementSystemAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeManagementSystemAPI.Repository
{
    public class EmployeeManagementRepository
    {
        private EmployeeDbContext _context;
        public EmployeeManagementRepository(EmployeeDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Employees>> GetAllEmployeeList()
        {
            try
            {
                var employees = await _context.Employees.Where(s => s.StaffId != 0).ToListAsync();
                return employees;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> UpdateEmployee(Employees emp)
        {
            try
            {
                _context.Update(emp);
                _context.Entry(emp).Property(x => x.StaffId).IsModified = false;
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                string s = ex.Message;
                return false;
            }
        }

        public async Task<bool> AddEmployee(Employees Emp)
        {
            _context.Employees.Add(Emp);
            await _context.SaveChangesAsync();
            return true;
        }

        public bool DeleteEmployee(int EmpId)
        {
            Employees emp = _context.Employees.Where(x => x.StaffId == EmpId).FirstOrDefault();

            if (emp != null)
            {
                _context.Employees.Remove(emp);
                _context.SaveChangesAsync();
                return true;
            }

            return false;
        }
    }
}
