﻿using EmployeeManagementSystemAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeManagementSystemAPI.Repository
{
    public class EducationalLevelRepository
    {
        private readonly EmployeeDbContext _context;
        public EducationalLevelRepository(EmployeeDbContext context)
        {
            _context = context;
        }

        public async Task<bool> UpdateEducationLevel(EducationLevel Elevel)
        {
            try
            {
                _context.Update(Elevel);
                _context.Entry(Elevel).Property(x => x.EducationLevelId).IsModified = false;
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                string s = ex.Message;
                return false;
            }
        }

        public async Task<bool> AddEducationLevel(EducationLevel Elevel)
        {
            _context.EducationLevel.Add(Elevel);
            await _context.SaveChangesAsync();
            return true;
        }

        public bool DeleteEducationalLevel(int eduID)
        {
            EducationLevel edu = _context.EducationLevel.Where(x => x.EducationLevelId == eduID).FirstOrDefault();
            if (edu != null)
            {
                _context.EducationLevel.Remove(edu);
                _context.SaveChangesAsync();
                return true;
            }
            return false;
        }
    }
}
