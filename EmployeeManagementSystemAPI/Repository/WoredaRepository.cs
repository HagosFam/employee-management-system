﻿using EmployeeManagementSystemAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeManagementSystemAPI.Repository
{
    public class WoredaRepository
    {
        private readonly EmployeeDbContext _context;
        public WoredaRepository(EmployeeDbContext context)
        {
            _context = context;
        }

        public async Task<bool> UpdateWoreda(Woreda woreda)
        {
            try
            {
                _context.Update(woreda);
                _context.Entry(woreda).Property(x => x.WoredaId).IsModified = false;
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                string s = ex.Message;
                return false;
            }
        }

        public async Task<bool> AddWoreda(Woreda woreda)
        {
            _context.Woreda.Add(woreda);
            await _context.SaveChangesAsync();
            return true;
        }

        public bool DeleteWoreda(int WoreId)
        {
            Woreda wore = _context.Woreda.Where(x => x.WoredaId == WoreId).FirstOrDefault();

            if (wore != null)
            {
                _context.Woreda.Remove(wore);
                _context.SaveChangesAsync();
                return true;
            }
            return false;
        }
    }
}
