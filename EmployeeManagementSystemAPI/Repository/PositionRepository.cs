﻿using AutoMapper;
using EmployeeManagementSystemAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeManagementSystemAPI.Repository
{
    public class PositionRepository
    {
        private readonly EmployeeDbContext _context;


        public PositionRepository(EmployeeDbContext context, IMapper mapper)
        {
            _context = context;
        }

        public async Task<bool> UpdatePosition(Position position)
        {
            try
            {
                _context.Update(position);
                _context.Entry(position).Property(x => x.PositionId).IsModified = false;
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                string s = ex.Message;
                return false;
            }
        }
        public async Task<bool> AddPosition(Position position)
        {
            _context.Position.Add(position);
            await _context.SaveChangesAsync();
            return true;
        }
      
        public bool DeletePosition(int PositionId)
        {
            Position position = _context.Position.Where(x => x.PositionId == PositionId).FirstOrDefault();

            if (position != null)
            {
                _context.Position.Remove(position);
                _context.SaveChangesAsync();
                return true;
            }

            return false;
        }


    }
}
