﻿using EmployeeManagementSystemAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeManagementSystemAPI.Repository
{
    public class NationalityRepository
    {
        private readonly EmployeeDbContext _context;
        public NationalityRepository(EmployeeDbContext context)
        {
            _context = context;
        }
        public async Task<bool> UpdateNationality(Nationality nation)
        {
            try
            {
                _context.Update(nation);
                _context.Entry(nation).Property(x => x.NationalityId).IsModified = false;
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                string s = ex.Message;
                return false;
            }
        }

        public async Task<bool> AddNationality(Nationality nation)
        {
            _context.Nationality.Add(nation);
            await _context.SaveChangesAsync();
            return true;
        }

        public bool DeleteNationality(int nationId)
        {
            Nationality nation = _context.Nationality.Where(x => x.NationalityId == nationId).FirstOrDefault();

            if (nation != null)
            {
                _context.Nationality.Remove(nation);
                _context.SaveChangesAsync();
                return true;
            }
            return false;
        }
    }
}
