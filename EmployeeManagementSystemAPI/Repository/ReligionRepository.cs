﻿using EmployeeManagementSystemAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeManagementSystemAPI.Repository
{
    public class ReligionRepository
    {
        private readonly EmployeeDbContext _context;
        public ReligionRepository(EmployeeDbContext context)
        {
            _context = context;
        }
        public async Task<bool> UpdateReligion(Religion rel)
        {
            try
            {
                _context.Update(rel);
                _context.Entry(rel).Property(x => x.ReligionId).IsModified = false;
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                string s = ex.Message;
                return false;
            }
        }

        public async Task<bool> AddReligion(Religion rel)
        {
            _context.Religion.Add(rel);
            await _context.SaveChangesAsync();
            return true;
        }

        public bool DeleteReligion(int RelId)
        {
            Religion rel = _context.Religion.Where(x => x.ReligionId == RelId).FirstOrDefault();

            if (rel != null)
            {
                _context.Religion.Remove(rel);
                _context.SaveChangesAsync();
                return true;
            }
            return false;
        }
    }
}
