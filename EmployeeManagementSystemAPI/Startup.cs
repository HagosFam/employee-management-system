using EmployeeManagementSystemAPI.Models;
using EmployeeManagementSystemAPI.Repository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeManagementSystemAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.WithOrigins($"http://localhost:4200", $"http://localhost:4400", $"http://192.168.254.7:60200", $"http://www.eca.com", $"http://192.168.8.6:60330", $"http://localhost:60330", $"http://192.168.8.6:60200", $"http://localhost:60200", $"http://127.0.0.1:60200")
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials());
            });
            services.AddSwaggerGen(c=>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Employee Management API", Version="V1" });

            });

            services.AddAutoMapper(typeof(Startup));
            services.AddDbContext<EmployeeDbContext>(optionns =>
            optionns.UseSqlServer(Configuration.GetConnectionString("DevConnection")));
            services.AddScoped<DepartmentRepository>();
            services.AddScoped<EducationalLevelRepository>();
            services.AddScoped<EmployeeManagementRepository>();
            services.AddScoped<KebelleRepository>();
            services.AddScoped<NationalityRepository>();
            services.AddScoped<PositionRepository>();
            services.AddScoped<RegionRepository>();
            services.AddScoped<ReligionRepository>();
            services.AddScoped<WoredaRepository>();
            services.AddScoped<ZoneRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
           
            app.UseSwagger();
            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "ICERS API");
            });



            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseCors("CorsPolicy");
            app.UseStaticFiles();
            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
